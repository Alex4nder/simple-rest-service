package template.restapi

import com.softwaremill.sttp._
import org.scalatest.{Matchers, WordSpec}

class ApplicationTest extends WordSpec with Matchers{
  "Service" should {
    "Response on port 8000" in {
      implicit val backend: SttpBackend[Id, Nothing] = HttpURLConnectionBackend()
      Application.start()
      sttp.get(uri"http://localhost:8000/status").send().code shouldBe 200
    }
  }
}
