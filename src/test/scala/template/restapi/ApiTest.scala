package template.restapi

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{Matchers, WordSpec}

class ApiTest extends WordSpec with Matchers with ScalatestRouteTest {
  "API" should {
    val routes = (new Api).routes
    "GET /status response OK" in {
      Get("/status") ~> routes ~> check {
        status shouldBe StatusCodes.OK
      }
    }

    "GET /rest/v2/items/get response OK" in {
      Get("/rest/v2/items/get") ~> routes ~> check {
        responseAs[String] shouldBe "{\"id\":42,\"name\":\"thing\"}"
      }
    }

    "POST /rest/v2/items/insert response OK" in {
      Post("/rest/v2/items/insert") ~> Route.seal(routes) ~> check {
        responseAs[String] shouldBe "[{\"items\":[{\"name\":\"thing2\",\"id\":43}]}]"
      }
    }
  }
}
