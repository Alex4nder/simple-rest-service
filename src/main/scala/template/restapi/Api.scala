package template.restapi

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json._

// domain model
final case class Item(name: String, id: Long)
final case class Order(items: List[Item])

// collect your json format instances into a support trait:
trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val itemFormat = jsonFormat2(Item)
  implicit val orderFormat = jsonFormat1(Order) // contains List[Item]
}

class Api extends Directives with JsonSupport {
  def routes : Route = path("status") {
    get {
      complete(StatusCodes.OK)
    }
  } ~ // Gluing routes. If the request does not meet the GET conditions, then process further
  pathPrefix ("rest" / "v2" / "items") { // Needed to shorten the request route
    path("get")
    {
      get {
        complete(Item("thing", 42)) // will render as JSON
      }
    } ~ /* use curl -H "Content-Type: application/json" -X POST -d '{"items":[{"name":"thing2","id":43}]}' http://localhost:8000/rest/v2/items/insert */
      path("insert") {
        post {
          entity(as[Order]) { order => // will unmarshal JSON to Order
            val itemsCount = order.items.size
            val itemNames = order.items.map(_.name).mkString(", ")
            complete(s"Ordered $itemsCount items: $itemNames")
          }
        }
      }
  }
}
